class Main{

    p = 0;

    constructor( p =0) {
        this.p = p;
    }

    setP(p = 0){ this.p = p;}

    getP(){return this.p;}

    add(q=0){ return this.p + q; }

    mul(q=0){ return this.p * q; }

    pow( p = 0 ){
        let out = 1;
        if(p<=0)return 0;
        for( let i = 0; i < p; i++ ) out *= this.p;

        return out;
    }

    static main( value =  [] ) {
        let main = new Main(2);
        Main.assert(main.add(8)===10,"Failed");
        Main.assert(main.pow(5)===32,"Failed "+main.pow(5));
    }

    static assert( condition, message = "") {
        if(!condition) throw new Error("assert false : "+message);
    }
}

Main.main([]);