# project-git-v1

<img src="https://img.shields.io/badge/Git-projectGitV1-green">

Ce projet à pour but d'appréhender le logiciel de versioning Git.

- Installation
- Licence

## Installation

Ce projet étant au format texte, aucune installation au préalable sera nécessaire.

## Licence 

Ce projet à une licence CC-0, libre accès et de partage.

## Authors

- Bob
- ZenityAcademy 
